const hello = () => {
  console.log('Hola!');
}

const bye = () => {
  console.log('Adios!');
}

module.exports = {
  hello,
  bye,
}